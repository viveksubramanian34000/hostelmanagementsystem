/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import static java.awt.Color.blue;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import static javafx.scene.paint.Color.color;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author vivek
 */
@WebServlet(urlPatterns = {"/regis"})
public class regis extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter(); 
        
        String sname=request.getParameter("sname");
        String lname=request.getParameter("lname");
        String gender=request.getParameter("gender");
        String birth=request.getParameter("bday");
        String address=request.getParameter("address");
        int pincode=Integer.parseInt(request.getParameter("pin"));
        long phone=Integer.parseInt(request.getParameter("pnno"));
        String email=request.getParameter("email");
        String regno=request.getParameter("regno");
        int batch=Integer.parseInt(request.getParameter("batch"));
        int cy=Integer.parseInt(request.getParameter("year"));
        String depart=request.getParameter("department");
        
        try
        {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            Connection con=DriverManager.getConnection("jdbc:derby://localhost:1527/registration","vivek", "vivek");
            Statement s=con.createStatement();
            String g="insert into registers values('"+sname+"','"+lname+"','"+gender+"','"+birth+"','"+address+"',"+pincode+","+phone+",'"+email+"','"+regno+"',"+batch+","+cy+",'"+depart+"')";
            //out.println("before updation");
            int d=s.executeUpdate(g);
            //out.println("no of rows updated"+d);
            
            out.print("<center ><h1>RESULT :</h1></center>");
            ResultSet rs = s.executeQuery("select * from registers");
            //out.println("after rs statement");
            ResultSetMetaData rsmd=rs.getMetaData();
            while(rs.next())
            {
                out.print("<table width=35% border=1>");
                out.print("<tr>");
                out.print("<td>"+rsmd.getColumnName(1)+"</td>");
            out.println("<td>"+rs.getString(1)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(2)+"</td>");
            out.println("<td>"+rs.getString(2)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(3)+"</td>");
            out.println("<td>"+rs.getString(3)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(4)+"</td>");
            out.println("<td>"+rs.getDate(4)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(5)+"</td>");
            out.println("<td>"+rs.getString(5)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(6)+"</td>");
            out.println("<td>"+rs.getInt(6)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(7)+"</td>");
            out.println("<td>"+rs.getLong(7)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(8)+"</td>");
            out.println("<td>"+rs.getString(8)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(9)+"</td>");
            out.println("<td>"+rs.getString(9)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(10)+"</td>");
            out.println("<td>"+rs.getInt(10)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(11)+"</td>");
            out.println("<td>"+rs.getInt(11)+"</td></tr>");
            
             out.print("<tr><td>"+rsmd.getColumnName(12)+"</td>");
            out.println("<td>"+rs.getString(12)+"</td></tr>");
            
            out.print("</table>");
            out.print("<br><br>");
            }
        }
        catch(Exception e)
        {
            out.println(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
