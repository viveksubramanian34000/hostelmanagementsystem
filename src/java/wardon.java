/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author vivek
 */
@WebServlet(urlPatterns = {"/wardon"})
public class wardon extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String idno=request.getParameter("idno");
        
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/wardon", "vivek", "vivek");
            Statement s=con.createStatement();
            out.print("<center><h1>RESULT :</h1></center>");
            ResultSet rs=s.executeQuery("select * from wardet where id='"+idno+"'");
            ResultSetMetaData rsmd=rs.getMetaData();
            while(rs.next())
            {
               out.print("<table width=30% border=1>");
                out.print("<tr><td>"+rsmd.getColumnName(1)+"</td>");
            out.println("<td>"+rs.getString(1)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(2)+"</td>");
            out.println("<td>"+rs.getString(2)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(3)+"</td>");
            out.println("<td>"+rs.getString(3)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(4)+"</td>");
            out.println("<td>"+rs.getString(4)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(5)+"</td>");
            out.println("<td>"+rs.getInt(5)+"</td></tr>");
            
            out.print("<tr><td>"+rsmd.getColumnName(6)+"</td>");
            out.println("<td>"+rs.getInt(6)+"</td></tr>");
            
            out.print("</table>");
            out.print("<br><br>");
            }
            
        }
        catch(Exception e){
            out.println(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
